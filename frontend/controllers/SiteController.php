<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\TestPerson;
use common\models\TestAnswer;
use yii\helpers\Url;
use common\models\search\TestPersonSearch;
use common\models\search\TestAnswerSearch;
use common\models\search\TestAnswerDetailSearch;

/**
 * Site controller
 */
class SiteController extends Controller
{   
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function redirect($url, $statusCode = 302)
    {
        // calling Url::to() here because Response::redirect() modifies route before calling Url::to()
        return $this->response->redirect(Url::to($url, URL_SCHEME ?? false), $statusCode);
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex($action = null)
    {   
		if (Yii::$app->user->isGuest) {
            if (!$action) {
                //return $this->redirect(['index', 'action' => 1]);
                $languagePrefix = Yii::$app->language === 'en-US' ? 'en' : 'ru';
                $this->layout = 'main_pre_index';
                return $this->render('pre_index', ['languagePrefix' => $languagePrefix]);
            }
            $testPersonArray = [];
            if (Yii::$app->request->isGet) {
                $testPersonArray = [
                    'surname' => Yii::$app->request->get('surname'),
                    'age' => Yii::$app->request->get('age'),
                    'gender' => Yii::$app->request->get('gender'),
                    'country' => Yii::$app->request->get('country'),
                    'level_of_education' => Yii::$app->request->get('level_of_education'),
                    'type_of_education' => Yii::$app->request->get('type_of_education'),
                    'is_art_education' => Yii::$app->request->get('is_art_education'),
                    'type_of_device' => Yii::$app->request->get('type_of_device'),
                    'color_blindness' => Yii::$app->request->get('color_blindness'),                
                ];
            }
            
            
			$model = new TestPerson($testPersonArray);

			if (
                $model->load(Yii::$app->request->post())
                && $model->validate()
                && $model->save()) {
                    Yii::$app->user->login($model, 3600 * 24 * 30);
					return $this->redirect(['before-test']);
			} elseif ($model->hasErrors()) {
				Yii::$app->session->setFlash('error', 'Something went wrong.');
			}

			return $this->render('index', ['model' => $model]);
		} else {
			if (Yii::$app->user->identity->isTestFinished) {
				return $this->redirect(['congratulations']);
			}
			return $this->redirect(['before-test']);
		}
    }

	public function actionLogout()
    {
		if (!Yii::$app->user->isGuest) {
			Yii::$app->user->logout();
		}
        return $this->redirect(['index']);
    }
	
	public function actionCongratulations()
    {
		if (Yii::$app->user->isGuest) {
			return $this->redirect(['index']);
		}
		
		$testPerson = Yii::$app->user->identity;
		
        Yii::$app->params['vk_button'] = true;
        return $this->render('congratulations', ['testPerson' => $testPerson]);
    }
	
	public function actionBeforeTest()
    {
		if (Yii::$app->user->isGuest) {
			return $this->redirect(['index']);
		}
		
		$testPerson = Yii::$app->user->identity;
		
        return $this->render('beforeTest', ['testPerson' => $testPerson]);
    }
	
	public function actionTestme($token = null)
    {
		if (Yii::$app->user->isGuest) {
			/* login if no session and verify token */
			if ($testPerson = TestPerson::findIdentityByAccessToken($token)) {
				Yii::$app->user->login($testPerson, 3600 * 24 * 30);
			}
			return $this->redirect(['site/index']);
		} else {
			$testPerson = Yii::$app->user->identity;
			
			if ($token !== $testPerson->personal_token) {
				$this->redirect(['site/testme', 'token' => $testPerson->personal_token]);
			}
            
            $testVersion = Yii::$app->params['testVersion'];
            $imgPrefix = $testVersion === 'anime' ? 'a' : 'p';
			
			$questionNumber = $testPerson->arrayOfQuestions[$testPerson->number_of_answer];
			$nextQuestionNumber = $testPerson->arrayOfQuestions[$testPerson->number_of_answer+1] ?? 0;
            $dopStim0Number = $testPerson->arrayOfDopStim0[$testPerson->number_of_answer];
            $dopStim1Number = $testPerson->arrayOfDopStim1[$testPerson->number_of_answer];
            $dopStim11Number = $testPerson->arrayOfDopStim11[$testPerson->number_of_answer];
            
            $nextDopStim0Number = $testPerson->arrayOfDopStim0[$testPerson->number_of_answer+1] ?? 0;
            $nextDopStim1Number = $testPerson->arrayOfDopStim1[$testPerson->number_of_answer+1] ?? 0;
            $nextDopStim11Number = $testPerson->arrayOfDopStim11[$testPerson->number_of_answer+1] ?? 0;
			
			if ($testPerson->number_of_answer >= 26) {
				return $this->redirect(['congratulations']);
			}
			
			if (Yii::$app->request->isPost) {
				if ($testPerson->number_of_answer !== (int) Yii::$app->request->post('number_of_answer')) {
					Yii::$app->session->setFlash('error', 'Something went wrong. Pls try again. (Error #12)');
					return $this->redirect(['site/testme', 'token' => $testPerson->personal_token]);
				}
				
				$model = new TestAnswer([
					'test_person_id' => $testPerson->id,
					'answer_number' => $testPerson->number_of_answer,
					'question_number' => $questionNumber,
					'img_prefix' => $imgPrefix,
				]);
				
				if (
					$model->load(Yii::$app->request->post())
					&& $model->validate()
					&& $model->save()) {
						$testPerson->number_of_answer++;
						$testPerson->save(false);
						$questionNumber = $testPerson->arrayOfQuestions[$testPerson->number_of_answer];
						$nextQuestionNumber = $testPerson->arrayOfQuestions[$testPerson->number_of_answer+1] ?? 1;
                        
                        $dopStim0Number = $testPerson->arrayOfDopStim0[$testPerson->number_of_answer];
                        $dopStim1Number = $testPerson->arrayOfDopStim1[$testPerson->number_of_answer];
                        $dopStim11Number = $testPerson->arrayOfDopStim11[$testPerson->number_of_answer];
                        
                        $nextDopStim0Number = $testPerson->arrayOfDopStim0[$testPerson->number_of_answer+1] ?? 1;
                        $nextDopStim1Number = $testPerson->arrayOfDopStim1[$testPerson->number_of_answer+1] ?? 1;
                        $nextDopStim11Number = $testPerson->arrayOfDopStim11[$testPerson->number_of_answer+1] ?? 1;
						/* now return final render('testme') */
				} elseif ($model->hasErrors()) {
					Yii::$app->session->setFlash('error', 'Something went wrong. Pls try again. (Error #21)');
					/* now return final render('testme') */
				}
			}
			
			if ($testPerson->number_of_answer >= 26) {
				return $this->redirect(['congratulations']);
			}
			
			$model = new TestAnswer(['img_prefix' => $imgPrefix]);
            
            $this->view->params['token'] = $testPerson->personal_token;
			return $this->render('testme', [
				'model' => $model,
				'token' => $testPerson->personal_token,
				'questionNumber' => $questionNumber,
				'nextQuestionNumber' => $nextQuestionNumber,
                'dopStim0Number' => $dopStim0Number,
                'dopStim1Number' => $dopStim1Number,
                'dopStim11Number' => $dopStim11Number,
                
                'nextDopStim0Number' => $nextDopStim0Number,
                'nextDopStim1Number' => $nextDopStim1Number,
                'nextDopStim11Number' => $nextDopStim11Number,
                
				'answerNumber' => $testPerson->number_of_answer,
                'testVersion' => $testVersion,
			]);
		}
    }
    
    public function actionSecretAbout($model)
    {
        if ($model === 'person') {
            $searchModel = new TestPersonSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('secret_about_person', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
        if ($model === 'answer') {
            $searchModel = new TestAnswerSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            
            $searchModelDetail = new TestAnswerDetailSearch();
            $dataProviderDetail = $searchModelDetail->search(Yii::$app->request->queryParams);

            return $this->render('secret_about_answer', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                
                'searchModelDetail' => $searchModelDetail,
                'dataProviderDetail' => $dataProviderDetail,
            ]);
        }
        throw new \yii\web\HttpException(404);
    }
}
