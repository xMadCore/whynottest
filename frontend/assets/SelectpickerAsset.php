<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * SelectpickerAsset frontend
 */
class SelectpickerAsset extends AssetBundle
{
	public $sourcePath = '@vendor/snapappointments/bootstrap-select/dist';
	public $css = [
        'css/bootstrap-select.css',
    ];
    public $js = [
		'js/bootstrap-select.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}
