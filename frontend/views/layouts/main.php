<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap4\Html;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\widgets\Pjax;
use chillerlan\QRCode\QRCode;
use chillerlan\QRCode\QROptions;
use yii\helpers\Url;
use common\components\ImagePusher;
use common\components\JsBufferWidget;

AppAsset::register($this);
$isPhotoType = Yii::$app->params['testVersion'] === 'photo';

$t = function ($message) {
	return \Yii::t('frontend', $message);
};

$authToken = Url::to(['site/testme', 'token' => $this->params['token']], true);
$baseLink = Url::base('https');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style>
        body {
            background: <?= $isPhotoType ? '#3e3e3e' : '#CACACA' ?>;
            color: <?= $isPhotoType ? '#CACACA' : '#3e3e3e' ?>;
            overflow-y: scroll;
        }
        .bg-dark {
            background-color: <?= $isPhotoType ? '#CACACA' : '#3e3e3e' ?> !important;
        }
        .bg-dark a {
            color: <?= $isPhotoType ? '#3e3e3e' : '#CACACA' ?>;
        }
        div.qr-code {
            box-shadow: 0 0 0 6px <?= $isPhotoType ? '#007bff' : '#007bff' ?>;
        }
        div.qr-code::before {
            background: <?= $isPhotoType ? '#007bff' : '#007bff' ?>;
        }
    </style>
    
    <?php if (Yii::$app->params['vk_button'] || true) : ?>
        <?php if (Yii::$app->language === 'en-US') : ?>
        <!-- для Facebook и ВКонтакте (протокол Open Graph) -->
        <meta property="og:title" content="Test with a long title " />
        <meta property="og:description" content="The Test from the Laboratory of Human-Computer Interaction at Peter the Great St. Petersburg Polytechnic University." />
        <meta property="og:image" content="<?=$baseLink?>/images/bugs-08.png" />
        <meta property="og:url" content="<?= Url::to(['site/index'], 'https') ?>" />
        <meta property="og:site_name" content="The Test «Visual attractiveness of a filmframe»" />
        <!-- для Twitter -->
        <meta name="twitter:site" content="The Test «Visual attractiveness of a filmframe»" />
        <meta name="twitter:title" content="Passed the Test" />
        <meta name="twitter:description" content="The Test from the Laboratory of Human-Computer Interaction at Peter the Great St. Petersburg Polytechnic University." />
        <?php endif; ?>
        <?php if (Yii::$app->language === 'ru-RU') : ?>
        <!-- для Facebook и ВКонтакте (протокол Open Graph) -->
        <meta property="og:title" content="Тест c длинным названием" />
        <meta property="og:description" content="Тест от лаборатории человеко-компьютерного взаимодействия Санкт-Петербургского Политехнического университета Петра Великого." />
        <meta property="og:image" content="<?=$baseLink?>/images/bugs-08.png" />
        <meta property="og:url" content="<?= Url::to(['site/index'], 'https') ?>" />
        <meta property="og:site_name" content="Тест «Визуальная привлекательность кадра»" />
        <!-- для Twitter -->
        <meta name="twitter:site" content="Тест «Визуальная привлекательность кадра»" />
        <meta name="twitter:title" content="Прошёл тест" />
        <meta name="twitter:description" content="Тест от лаборатории человеко-компьютерного взаимодействия Санкт-Петербургского Политехнического университета Петра Великого." />
        <?php endif; ?>
    <?php endif; ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
    <div class="container">
        <?php
        function getFullUrl($array) {
            return [''] + array_merge($array, Yii::$app->request->get());
        }
        if (isset($this->params['token'])) {
            if (file_exists(Yii::getAlias('@frontend/web/qr_cache/qr_' . $this->params['token'] . '.png'))) {
                $qrCode = imageCreateFromPNG(Yii::getAlias('@frontend/web/qr_cache/qr_' . $this->params['token'] . '.png'));
            } else {
                $options = new QROptions([
                    'version'    => 8,
                    'outputType' => QRCode::OUTPUT_IMAGE_PNG,
                    'eccLevel'   => QRCode::ECC_H,
                    'scale' => 25,
                    'imageBase64' => false,
                    'imageTransparent' => false,
                ]);

                $qrCode = (new QRCode($options))->render(Url::to(['site/testme', 'token' => $this->params['token']], true));
                $qrCode = imagecreatefromstring($qrCode);

                $startX = 724-25*8;
                $startY = 724-25*8;

                $mask = imageCreateFromPNG(Yii::getAlias('@frontend/web/images/mask2.png'));

                $imagePusher = (new ImagePusher());
                $imagePusher->readMask($mask);
                $imagePusher->executeMask($startX, $startY, $qrCode, 0.5);

                $stitch = imagecreatefrompng(Yii::getAlias('@frontend/web/images/626_01_rotate2.png'));
                imagecopy($qrCode, $stitch, 650, 650, 0, 0, 426, 402);

                //$qrCode = imagescale($qrCode, 400);
                imagepng($qrCode, Yii::getAlias('@frontend/web/qr_cache/qr_' . $this->params['token'] . '.png'));
                imagedestroy($qrCode);
            }

            $qrCode = '/qr_cache/qr_' . $this->params['token'] . '.png';
        }
        
        ?>
		<?php NavBar::begin(['options' => ['class' => 'navbar navbar-dark bg-dark navbar-expand']]); ?>
        <?= Nav::widget([
            'items' => [
                [
                    'label' => $t('I\'m tired :/'),
                    'url' => '#profile',
                    'active' => false,
                    'visible' => isset($this->params['token']),
                    'linkOptions' => [
                        'id' => 'profile-tab',
                        'class' => 'hide-when-active',
                        'data-toggle' => 'tab',
                        'role' => 'tab',
                        'aria-controls' => 'profile',
                        'aria-selected' => 'false',
                    ],
                ],
                [
                    'label' => $t('Back to Test'),
                    'active' => true,
                    'url' => '#home',
                    'linkOptions' => [
                        'id' => 'home-tab',
                        'class' => 'hide-when-active',
                        'data-toggle' => 'tab',
                        'role' => 'tab',
                        'aria-controls' => 'home',
                        'aria-selected' => 'false',
                    ],
                ],
            ],
            'options' => ['class' => 'mx-auto'],
        ]) ?>
        <?= Nav::widget([
            'items' => [
                ['label' => 'RUS', 'url' => getFullUrl(['language' => 'ru']), 'active' => Yii::$app->language == 'ru-RU'],
                ['label' => 'ENG', 'url' => getFullUrl(['language' => 'en']), 'active' => Yii::$app->language == 'en-US'],
            ],
            'options' => ['class' => ''],
        ]) ?>
		<?php NavBar::end(); ?>
		<br>
        <?= Alert::widget() ?>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <?= $content ?>
            </div>
            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <p>
                    <?= $t('Then you can continue the test later or on another device.') ?>
                    <br>
                    <?= $t('This is your super secret (and incredibly personal) link! After going through it, you will continue from the same place where you left off.') ?>
                </p>
                <input type="text" value="<?= $authToken ?>" id="copyInput" style="opacity: 0; width:1px;">
                <?= Html::button($t('тык! For copy'), ['class' => 'btn btn-link', 'onclick' => 'copyToBuffer()']) ?>
                <?= Html::a($authToken, $authToken, ['class' => 'btn btn-link', 'onclick' => 'copyToBuffer()']) ?>
                <p>
                    <?= $t('Could actually be copied from the search bar, this works too.') ?>
                    <br>
                    <?= $t('It is also included in the QR code, if you want to scan it.') ?>
                </p>
                <br>
                <?= Html::tag('div', '' . Html::tag('div', Html::img($qrCode ?? '', ['class' => 'qr-code']), ['class' => 'qr-code'])) ?>
            </div>
        </div>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
