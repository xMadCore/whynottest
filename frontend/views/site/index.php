<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model common\models\TestPerson */

$t = function ($message) {
	return \Yii::t('frontend', $message);
};

$this->title = 'Why Not Test';
?>
<h1 class="wf-animate-bottom_to_top1" style="text-align: center;"><?= $t('Online test «Visual <span class="wf_smile">attractiveness</span> of the filmframe»') ?></h1>
<div class="row justify-content-center wf-animate-bottom_to_top2">
	<?php $form = ActiveForm::begin([
		'id' => 'form-test-person',
		'options' => [
			'class' => 'col-10 col-lg-10 col-sm-8 ',
		],
		'fieldConfig' => ['template' => "{label}\n<div style=\"min-height: 81px;\">{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}</div>"],
	]); ?>
	<p class="text-center"><?= $t('It is preferable To pass the test, to use a personal computer or laptop.')?>
    <br><br><h4 class="text-center"><?= $t('Fill in information about yourself:') ?></h4>
    <p class="text-center">(<?= $t('The data from this questionnaire will be used exclusively for scientific research. Your data is not disseminated and is not publicly available. The questionnaire can be filled out anonymously with a fictitious nickname instead of a name. Gender, age and educational information must be correct.') ?>)</p>
	<div class="row">
		<div class="col-lg-4 wf-animate-bottom_to_top3">
			<?= $form->field($model, 'surname')->textInput() ?>
            <?= $form->field($model, 'age')->dropDownList($model->agesArray, ['prompt' => $t('Choose...')]) ?>
			<?= $form->field($model, 'gender')->dropDownList($model->gendersArray, ['prompt' => $t('Choose...')]) ?>
		</div>
		<div class="col-lg-4 wf-animate-bottom_to_top4">
			<?= $form->field($model, 'level_of_education')->dropDownList($model->levelsOfEducationArray, ['prompt' => $t('Choose...'), 'onchange' => 'onChangeEducation(event)']) ?>
			<?= $form->field($model, 'type_of_education')->dropDownList($model->typesOfEducationArray, ['prompt' => $t('Choose...')]) ?>
			<?= $form->field($model, 'is_art_education')->dropDownList($model->yesNoArray, ['prompt' => $t('Choose...')]) ?>
		</div>
        <div class="col-lg-4 wf-animate-bottom_to_top5">
            <?= $form->field($model, 'country')->dropDownList($model->countriesArray, ['prompt' => $t('Choose...')]) ?>
            <?= $form->field($model, 'color_blindness')->dropDownList($model->colorBlindnessArray, ['prompt' => $t('Choose...')]) ?>
            <?= $form->field($model, 'type_of_device')->dropDownList($model->devicesArray, ['prompt' => $t('Choose...')]) ?>
        </div>
	</div>
	<div class="w-100"></div>
	<div class="row">
		<div class="col-12 text-center">
			<?= Html::submitButton($t('Continue'), ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
		</div>
	</div>
    <br>
    <div class="row">
        <p>
            <?= $t('This testing is part of a scientific study of the effect of frame elements on the perception of visual information and is carried out by researchers from')?> 
            <?= Html::a($t('the laboratory of human-computer interaction at Peter the Great St. Petersburg Polytechnic University.'), 'https://design.spbstu.ru/laboratory/human_computer/', ['target' => '_blank'])?>
        </p>
    </div>
	<?php ActiveForm::end(); ?>
</div>