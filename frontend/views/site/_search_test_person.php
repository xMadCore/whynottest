<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TestPersonSearch */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="test-person-search">

    <?php $form = ActiveForm::begin([
        //'action' => ['secret-about'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'surname') ?>

    <?= $form->field($model, 'age') ?>

    <?= $form->field($model, 'gender') ?>

    <?= $form->field($model, 'country') ?>

    <?= Html::activeHiddenInput($model, 'level_of_education') ?>
    
    <?= Html::activeHiddenInput($model, 'type_of_education') ?>
    
    <?= Html::activeHiddenInput($model, 'is_art_education') ?>
    
    <?= Html::activeHiddenInput($model, 'type_of_device') ?>
    
    <?= Html::activeHiddenInput($model, 'array_of_questions_json') ?>
    
    <?= Html::activeHiddenInput($model, 'number_of_answer') ?>
    
    <?= Html::activeHiddenInput($model, 'personal_token') ?>
    
    <?= Html::activeHiddenInput($model, 'auth_key') ?>
    
    <?= Html::activeHiddenInput($model, 'created_at') ?>
    
    <?= Html::activeHiddenInput($model, 'updated_at') ?>
    
    <?= Html::activeHiddenInput($model, 'is_deleted') ?>
    
    <?= Html::activeHiddenInput($model, 'deleted_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('frontend', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('frontend', 'Reset'), ['model' => 'person'], ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
