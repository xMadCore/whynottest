<?php

use yii\grid\GridView;
use yii\data\ArrayDataProvider;

/* @var $this yii\web\View */

$this->title = 'Secret About';

?>
<div class="site-about">
	<?php
		$provider = new ArrayDataProvider([
			'allModels' => common\models\TestPerson::find()->asArray()->all(),
			'pagination' => [
				'pageSize' => 100,
			],
		]);

		echo GridView::widget([
			'dataProvider' => $provider,
			'tableOptions' => ['class' => 'table table-bordered']
		]);
		
		echo '<br><br>';
		
		$provider = new ArrayDataProvider([
			'allModels' => common\models\TestAnswer::find()->asArray()->all(),
			'pagination' => [
				'pageSize' => 100,
			],
		]);

		echo GridView::widget([
			'dataProvider' => $provider,
			'tableOptions' => ['class' => 'table table-bordered']
		]);
	?>
    <code><?= __FILE__ ?></code>
</div>
