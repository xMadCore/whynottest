<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\components\JsBufferWidget;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;
use common\components\BsImg;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TestPersonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('frontend', 'Test Peoples');
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    'id' => [
        'attribute' => 'id',
        'filter' => false,
    ],
    'surname',
    'ageString' => [
        'attribute' => 'age',
        'value' => 'ageString'
    ],
    'gender',
    'country',
    'level_of_education',
    'type_of_education',
    'is_art_education',
    'color_blindness',
    'type_of_device',
    [
        'attribute' => 'array_of_questions_json',
        'contentOptions' => [
            //'class' => 'hover-overflow-x',
        ],
        'format' => 'html',
        'value' => function ($model) {
            return "<div class=\"hover-overflow-x\"><div class=\"hover-overflow-x-content\">$model->array_of_questions_json</div></div>";
        }
    ],
    [
        'attribute' => 'array_of_dop_stim_0_json',
        'format' => 'html',
        'value' => function ($model) {
            return "<div class=\"hover-overflow-x\"><div class=\"hover-overflow-x-content\">$model->array_of_dop_stim_0_json</div></div>";
        }
    ],
    [
        'attribute' => 'array_of_dop_stim_1_json',
        'format' => 'html',
        'value' => function ($model) {
            return "<div class=\"hover-overflow-x\"><div class=\"hover-overflow-x-content\">$model->array_of_dop_stim_1_json</div></div>";
        }
    ],
    [
        'attribute' => 'array_of_dop_stim_1_1_json',
        'format' => 'html',
        'value' => function ($model) {
            return "<div class=\"hover-overflow-x\"><div class=\"hover-overflow-x-content\">$model->array_of_dop_stim_1_1_json</div></div>";
        }
    ],
    'number_of_answer',
    'personal_token',
    'auth_key',
    'created_at:datetime',
    'updated_at:datetime',
    'is_deleted',
    'deleted_at:datetime',
];
    
$collapseFilters = [
    'id' => null,
    'created_at' => null,
    'deleted_at' => null,
];
$collapsedFilter = empty(array_intersect_key($collapseFilters, $searchModel->getDirtyAttributes()));
?>
<div class="test-person-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?= Html::button(Yii::t('app', 'Show all filters'), [
        'class' => 'btn btn-light',
        'data-toggle' => "collapse",
        'data-target' => "#collapseFilters",
        'aria-expanded' => $collapsedFilter ? false : true,
        'aria-controls' => "collapseFilters",
    ]) ?>
    
    <?php Pjax::begin(); ?>
    <?php JsBufferWidget::begin(); ?>
    
    <div id="collapseFilters" class="collapse<?=$collapsedFilter ? '' : ' show'?>">
        <div class="card card-body container-fluid">
            <?php echo $this->render('_search_test_person', ['model' => $searchModel]); ?>
        </div>
    </div>

    <?= $a = ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'showConfirmAlert' => false,
        'filename' => date('Y-m-d H:i:s') . '-test_person_export',
        'fontAwesome' => false,
        'dropdownOptions' => ['icon' => BsImg::getSvg('download')],
        'columnSelectorOptions' => ['icon' => BsImg::getSvg('list-task')]
    ]) ?>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
    ]); ?>

    <?php JsBufferWidget::end(); ?>
    <?php Pjax::end(); ?>

</div>
