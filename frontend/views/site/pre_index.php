<?php

use yii\bootstrap4\Html;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model common\models\TestPerson */

$t = function ($message) {
	return \Yii::t('frontend', $message);
};

$this->title = 'Why Not Test';
?>
<h1 class="wf-animate-top_to_bottom" style="text-align: center;"><?= $t('Choose which test you want to start:') ?></h1>
