<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $testPerson common\models\TestPerson */

$t = function ($message, $params = []) {
	return \Yii::t('frontend', $message, $params);
};

$this->title = $t('Pass the test «Visual attractiveness of a filmframe»');

$user = Yii::$app->user->identity;
$isAnime = Yii::$app->params['testVersion'] === 'anime';
$anotherImageLink = $isAnime ? '/images/bugs-07.png' : '/images/bugs-06.png';
$languagePrefix = Yii::$app->language === 'en-US' ? '/en/' : '/ru/';
$anotherTestLink = $isAnime ? Yii::$app->params['link.photo'] : Yii::$app->params['link.anime'];
$anotherTestLink = str_replace('/en/', $languagePrefix, $anotherTestLink);
$anotherTestLink .= "&surname=$user->surname"
    . "&age=$user->age"
    . "&gender=$user->gender"
    . "&country=$user->country"
    . "&level_of_education=$user->level_of_education"
    . "&type_of_education=$user->type_of_education"
    . "&is_art_education=$user->is_art_education"
    . "&type_of_device=$user->type_of_device"
    . "&color_blindness=$user->color_blindness";
$baseLink = Url::base('https');
?>
<div class="row justify-content-center">
	<div class="col-xs-6 col-xs-6" style="text-align: right">
        <br>
        <h5>
            <b><?= $t('Congratulations, you did it successfully.') ?></b>
		</h5>
		<h4>
			<b><?= $t('Test complete!') ?></b>
		</h4>
		<h4>
			<b><?= $t('Thank you for your patience!') ?></b>
		</h4>
        <h5>
            <?= $t('The Yandex Toloka verification code:') ?> <b class="wf_smile" style="color:#e34a4a">239</b>
        </h5>
	</div>
	<div class="col-xs-6 col-xs-6" >
		<?= Html::img('/images/626_01.png', ['height' => '160px']) ?>
	</div>
</div>
<br>
<div class="row">
    <p>
        <?= $t('You are such a great fellow that you coped with this test, if you still have strength, please take one more test') ?>
    </p>
    <div class="text-center w-100">
        <?= Html::a(Html::img($anotherImageLink, ['width' => '60%']), $anotherTestLink) ?>
        <br>
        <br>
        <?= Html::a('<b>' . $t('Pass second test') . '</b>', $anotherTestLink, ['class' => 'btn btn3d btn-primary', 'style' => '']) ?>
    </div>
    <p>
        <br>
        <p>
            <?= $t('Or end the session and return to ') . Html::a($t('test choice'), ['/site/logout'], ['class' => 'btn btn-sm btn3d btn-danger', 'style' => 'margin-bottom: 3px; opacity:0.8;']) ?> 
        </p>
        <?= $t('We are grateful for your participation! Please share the link to the test with your friends, colleagues, relatives.') ?>
        <br>
        <?= $t('The obtained data will help in studying the visual attractiveness of the filmframe! Follow our publications on the page of') ?>
        <?= Html::a($t('the laboratory of human-computer interaction'), 'https://design.spbstu.ru/laboratory/human_computer/', ['target' => '_blank']) ?>
        <?php if (Yii::$app->params['vk_button']) : ?>
            <script src="https://yastatic.net/share2/share.js"></script>
            <div class="w-100 text-center">
                <?= $t('Share:') ?>
                <div class="ya-share2 " data-url="<?= Url::to(['site/index'], 'https') ?>" data-image="<?=$baseLink?>/images/bugs-08.png" data-curtain data-size="m" data-services="vkontakte,telegram,whatsapp,facebook,twitter"></div>
            </div>
        <?php endif; ?>
    </p>
</div>