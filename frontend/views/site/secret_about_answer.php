<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\components\JsBufferWidget;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;
use common\components\BsImg;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TestPersonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('frontend', 'Test Answers');
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    'id' => [
        'attribute' => 'id',
        'filter' => false,
    ],
    'test_person_id',
    'answer_number',
    'question_number',
    'created_at:datetime',
    'bww_rate',
    'dop_rate',
    'onc_rate',
    'onw_rate',
    'thr_rate',
    'answer_rate_1',
    'answer_rate_2',
    'answer_rate_3',
    'answer_rate_4',
    'answer_rate_5',
    'img_prefix'
];

$gridColumnsDetail = [
    'id' => [
        'attribute' => 'id',
        'filter' => false,
    ],
    'test_person_id',
    'test_answer_id',
    'answer_number',
    'question_number',
    'answer_name',
    'answer_rate',
    'img_prefix'
];
?>
<div class="test-person-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?php Pjax::begin(); ?>
    <?php JsBufferWidget::begin(); ?>
    
    <?php echo $this->render('_search_test_answer', ['model' => $searchModel]); ?>

    <?= $a = ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'showConfirmAlert' => false,
        'filename' => date('Y-m-d H:i:s') . '-test_answer_export',
        'fontAwesome' => false,
        'dropdownOptions' => ['icon' => BsImg::getSvg('download')],
        'columnSelectorOptions' => ['icon' => BsImg::getSvg('list-task')]
    ]) ?>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
    ]); ?>

    <?php JsBufferWidget::end(); ?>
    <?php Pjax::end(); ?>
    
    <?php Pjax::begin(); ?>
    <?php JsBufferWidget::begin(); ?>

    <?= $a = ExportMenu::widget([
        'dataProvider' => $dataProviderDetail,
        'columns' => $gridColumnsDetail,
        'showConfirmAlert' => false,
        'filename' => date('Y-m-d H:i:s') . '-test_answer_detail_export',
        'fontAwesome' => false,
        'dropdownOptions' => ['icon' => BsImg::getSvg('download')],
        'columnSelectorOptions' => ['icon' => BsImg::getSvg('list-task')]
    ]) ?>
    
    <?= GridView::widget([
        'dataProvider' => $dataProviderDetail,
        'filterModel' => $searchModelDetail,
        'columns' => $gridColumnsDetail,
    ]); ?>

    <?php JsBufferWidget::end(); ?>
    <?php Pjax::end(); ?>

</div>
