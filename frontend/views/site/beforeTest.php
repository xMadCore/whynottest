<?php

use yii\bootstrap4\Html;

/* @var $this yii\web\View */
/* @var $testPerson common\models\TestPerson */

$t = function ($message, $params = []) {
	return \Yii::t('frontend', $message, $params);
};

$this->title = $t('Before Test');

?>
<div class="row">
	<div class="col-12">
		<p>
			<?= $t('You are take a part in an experiment «Visual attractiveness of a filmframe» test.') ?>
		</p>
    </div>
    <div class="col-12">
        <div class="card col-12">
            <div class="card-body align-content-center" style="padding: 1rem;">
                <h5 style="margin-bottom: 0;"><?= $t('Completed {n} of 25', ['n' => $testPerson->number_of_answer-1]) ?></h5>
            </div>
        </div>
    </div>
    <div class="col-12">
		<p>
            <b><?= $t('Task') ?></b>
            <br>
            <?= $t('You will be offered 25 slides, each slide has 16 film frames.') ?>
            <br>
            <b><?= $t('It is necessary to select all frames with two centers of interest.') ?></b>
            <br>
            <?= $t('The center of interest is an object in the frame, representing a hero or several heroes united by one outline.') ?>
            <br>
            <?= $t('Proceed to the next slide by clicking the "Continue" button.') ?>
		</p>
    </div>
</div>
<?php
    $buttonText = $testPerson->number_of_answer >= 26 ? 'Reset and pass the test again' : 'Continue';
    $buttonText = $testPerson->number_of_answer == 1 ? 'Start test' : $buttonText;
    $buttonUrl = $testPerson->number_of_answer >= 26 ? ['/site/logout'] : ['/site/testme', 'token' => $testPerson->personal_token];
    $buttonClass = $testPerson->number_of_answer >= 26 ? 'btn-danger' : 'btn-primary';
    echo Html::a($t($buttonText), $buttonUrl, ['class' => "float-right btn btn3d $buttonClass"]);
?>