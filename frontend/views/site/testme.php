<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use common\models\TestAnswer;
use common\components\JsBufferWidget;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\TestAnswer */
/* @var $token string*/
/* @var $questionNumber integer */
/* @var $answerNumber integer */

$this->title = 'testME';

$t = function ($message, $params = []) {
	return \Yii::t('frontend', $message, $params);
};

$prefix = $testVersion === 'anime' ? 'a' : 'p';

$questionNumber = str_pad($questionNumber, 2, '0', STR_PAD_LEFT);

$nextPrefix = $testVersion === 'anime' ? 'a' : 'p';
$nextQuestionNumber = str_pad($nextQuestionNumber, 2, '0', STR_PAD_LEFT);

$dopStim0Number = str_pad($dopStim0Number, 2, '0', STR_PAD_LEFT);
$dopStim1Number = str_pad($dopStim1Number, 2, '0', STR_PAD_LEFT);
$dopStim11Number = str_pad($dopStim11Number, 2, '0', STR_PAD_LEFT);

$nextDopStim0Number = str_pad($nextDopStim0Number, 2, '0', STR_PAD_LEFT);
$nextDopStim1Number = str_pad($nextDopStim1Number, 2, '0', STR_PAD_LEFT);
$nextDopStim11Number = str_pad($nextDopStim11Number, 2, '0', STR_PAD_LEFT);

$clickImg = '{clickImg(this);}';

foreach(TestAnswer::getTypesArray() as $imageType) {
    $imageNames[] = ['name' => "stim_$testVersion/t{$prefix}$questionNumber{$imageType}2.jpg", 'type' => $imageType];
}

foreach(TestAnswer::getTypes0Array() as $imageType) {
    $dopStim0ImageNames[] = ['name' => "dop_stim_{$testVersion}_zero/f{$prefix}$dopStim0Number{$imageType}0.jpg", 'type' => "f{$prefix}$dopStim0Number{$imageType}0"];
}

foreach(TestAnswer::getTypes1Array() as $imageType) {
    $dopStim1ImageNames[] = ['name' => "dop_stim_$testVersion/f{$prefix}$dopStim1Number{$imageType}1.jpg", 'type' => "f{$prefix}$dopStim1Number{$imageType}1"];
}

foreach(TestAnswer::getTypes1Array() as $imageType) {
    $dopStim11ImageNames[] = ['name' => "dop_stim_$testVersion/f{$prefix}$dopStim11Number{$imageType}1.jpg", 'type' => "f{$prefix}$dopStim11Number{$imageType}1"];
}

$imageNames = array_merge($imageNames, $dopStim1ImageNames, $dopStim11ImageNames, $dopStim0ImageNames);

$qTestMatrix = false;
while (!$qTestMatrix) {
    shuffle($imageNames);
    for ($i = 0; $i < 16; $i++) {
        if ($i > 0 && substr($imageNames[$i]['name'],0,1) === 's' && substr($imageNames[$i]['name'],0,1) === substr($imageNames[$i-1]['name'],0,1)) {
            $qTestMatrix = false;
            break;
        }
        if ($i < 15 && substr($imageNames[$i]['name'],0,1) === 's' && substr($imageNames[$i]['name'],0,1) === substr($imageNames[$i+1]['name'],0,1)) {
            $qTestMatrix = false;
            break;
        }
        if ($i > 3 && substr($imageNames[$i]['name'],0,1) === 's' && substr($imageNames[$i]['name'],0,1) === substr($imageNames[$i-4]['name'],0,1)) {
            $qTestMatrix = false;
            break;
        }
        if ($i < 12 && substr($imageNames[$i]['name'],0,1) === 's' && substr($imageNames[$i]['name'],0,1) === substr($imageNames[$i+4]['name'],0,1)) {
            $qTestMatrix = false;
            break;
        }
        $qTestMatrix = true;
    }
}

?>
<?php Pjax::begin() ?>
<?php JsBufferWidget::begin(); ?>
<div class="text-center">
	<h4><?= $t('Select all images with two centers of interest') ?></h4>
	<br>
	<div class="row justify-content-center">
	<?php
		foreach($imageNames as $image_item) {
			//echo $image_item;
            echo Html::tag(
                'figure',
                Html::input('image', 'img-fluid', null, [
                    'src' => "/images/{$image_item['name']}",
                    'width' => '100%',
                ]),
                [
                    'class' => "img col-3 img-fluid align-self-center",
                    'onClick' => $clickImg,
                    'answer-type' => $image_item['type'],
                ]
            );
		}
	?>
	</div>
	<div class="row justify-content-center">
    <?php $form = ActiveForm::begin([
            'id' => 'form-test-answer',
            'fieldConfig' => ['template' => '{input}'],
            'options' => ['class' => 'col-12', 'data-pjax' => true]
		]); ?>
		<?= Html::hiddenInput('number_of_answer', $answerNumber); ?>
		<?= $form->field($model, 'stringAnswers')->hiddenInput()->label(false) ?>
		<div class="col-12 text-right justify-content-end">
				<h5><?= $t('Answer {n} of 25', ['n' => $answerNumber]) ?></h5>
		</div>
		<div class="col-12 justify-content-center">
			<div class="col text-center">
				<?= Html::button($t('Reset'), ['class' => 'btn btn-danger', 'onclick' => '(() => {next = resetNext();})()']) ?>
				&nbsp;&nbsp;
				<?= Html::submitButton($t('Continue'), ['class' => 'btn btn-primary', 'name' => 'submit-button']) ?>
			</div>
		</div>
        
	<?php ActiveForm::end(); ?>
	</div>
</div>

<div class="hide d-none" hidden style="display: none;">
	<?php
		foreach(TestAnswer::getTypesArray() as $imageType) {
			echo Html::input('image', 'img-fluid', null, [
                'src' => "/images/stim_$testVersion/t{$nextPrefix}$nextQuestionNumber{$imageType}2.jpg",
			]);
		}
        
        foreach(TestAnswer::getTypes0Array() as $imageType) {
            $name = "dop_stim_{$testVersion}_zero/f{$prefix}$nextDopStim0Number{$imageType}0.jpg";
            echo Html::input('image', 'img-fluid', null, [
                'src' => "/images/$name",
			]);
        }
        foreach(TestAnswer::getTypes1Array() as $imageType) {
            $name = "dop_stim_$testVersion/f{$prefix}$nextDopStim1Number{$imageType}1.jpg";
            echo Html::input('image', 'img-fluid', null, [
                'src' => "/images/$name",
			]);
        }
        foreach(TestAnswer::getTypes1Array() as $imageType) {
            $name = "dop_stim_$testVersion/f{$prefix}$nextDopStim11Number{$imageType}1.jpg";
            echo Html::input('image', 'img-fluid', null, [
                'src' => "/images/$name",
			]);
        }
	?>
</div>
<?php
    $reg = "$('#form-test-answer').on('afterValidate', (event, messages, errorAttributes) => {
        if (errorAttributes.length > 0) {
            console.warn(messages, errorAttributes);
            toggleModal('wrong-answer');
        }
    });";
    $this->registerJs($reg);
?>
<?php JsBufferWidget::end(); ?>
<?php Pjax::end() ?>
<div id="wrong-answer" class="wf-modal d-none" onclick="toggleModal('wrong-answer')">
    <div class="wf-modal-content wf-animate-bottom" onclick="event.stopPropagation();">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="float-right">
                        <a href="#!" onclick="toggleModal('wrong-answer')" data-bs-dismiss="modal" aria-label="Close"><img src="/images/close_icon.png" alt=""></a>
                    </div>
                    <div class="" style="color:black">
                        <?= $t('You did not select all frames with two centers of interest') ?>
                    </div>
                    <br>
                    <br>
                    <div class="container text-center">
                        <?= Html::button($t('Continue'), ['class' => 'btn btn-primary', 'name' => 'continue-button', 'onclick' => "toggleModal('wrong-answer')"]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>