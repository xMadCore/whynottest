<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TestPersonSearch */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="test-answer-search">

    <?php $form = ActiveForm::begin([
        //'action' => ['secret-about'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'test_person_id') ?>

    <?= Html::activeHiddenInput($model, 'answer_number') ?>

    <?= Html::activeHiddenInput($model, 'question_number') ?>

    <?= Html::activeHiddenInput($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('frontend', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('frontend', 'Reset'), ['model' => 'answer'], ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
