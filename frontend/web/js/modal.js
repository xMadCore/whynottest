'use strict';

function toggleModal(modalId) {
    let modal = document.getElementById(modalId);
    modal.classList.toggle('d-none');
}

function changePassword(modalId) {
    let oldInput = document.getElementById('settings-oldpassword').value;
    let newInput = document.getElementById('settings-newpassword').value;
    
    $.ajax({
        data: {
            token: token,
            old_password: oldInput,
            new_password: newInput,
        },
        type: 'POST',
        url: '/api/change-password',
        success: (data, status) => {
            console.log('Success change password', data, status);
            toggleModal(modalId);
        },
        error: (data, status) => {
            console.error('Error while change password', data, status);
        },
    });
}