function resetNext() {
    $('.selected').each((key, value) => {
        $(value).removeClass('selected');
        $('#testanswer-stringanswers').val('');
    });
}

function clickImg (that) {
    if (!$(that).hasClass('selected')) {
        $(that).addClass('selected');
        $('#testanswer-stringanswers').val($('#testanswer-stringanswers').val() + $(that).attr('answer-type') + ':');
    } else {
        $(that).removeClass('selected');
        $('#testanswer-stringanswers').val($('#testanswer-stringanswers').val().replace($(that).attr('answer-type') + ':', ''));
    }
};

resetNext();

function onChangeEducation (event) {
    if (event.target.value == 0) {
        $('#testperson-type_of_education').val(0).change();
    }
}

function copyToBuffer() {
  var copyText = document.getElementById("copyInput");
  copyText.select();
  document.execCommand("copy");
} 