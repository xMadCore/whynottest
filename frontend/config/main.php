<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
	'defaultRoute' => 'site/index',
    'modules' => [
		'gridview' => [
			'class' => '\kartik\grid\Module',
		],
	],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\TestPerson',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'sweet-cookie-why_not-ha',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
			'class' => 'codemix\localeurls\UrlManager',
			'languages' => ['ru' => 'ru-RU', 'en' => 'en-US'],
			'enablePrettyUrl' => true,
            'showScriptName' => false,
			'enableLanguageDetection' => false,
			'enableStrictParsing' => false,
			'enableDefaultLanguageUrlCode' => true,
            'rules' => [
				'<controller:[\w-]+>/<action:[\w-]+>/<token:[\w-]+>' => '<controller>/<action>',
            ],
        ],
		'i18n' => [
			'translations' => [
				'*' => [
					'class' => 'yii\i18n\PhpMessageSource',
					'basePath' => '@common/translations',
					'sourceLanguage' => 'en-US',
				],
			],
		],
    ],
    'params' => $params,
];
