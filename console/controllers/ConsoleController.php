<?php

namespace console\controllers;

use Yii;

class ConsoleController extends \yii\console\Controller
{
    public function actionFlushYiiCache()
    {
        return Yii::$app->cache->flush();
    }
    
    public function actionFlushQrCache()
    {
        $qrImageCacheDir = Yii::getAlias('@frontend/web/qr_cache');
        array_map('unlink', glob(Yii::getAlias("$qrImageCacheDir/*.png")));
    }
    
    public function actionFlushDebug($deleteDir = false)
    {
        $debugDir = Yii::getAlias('@frontend/runtime/debug');
        array_map('unlink', glob(Yii::getAlias("$debugDir/*")));
        if ($deleteDir) {
            @rmdir(Yii::getAlias($debugDir)) or die('php cant rm dir ');
        }
    }
}
