<?php

use yii\db\Migration;

/**
 * Class m210718_093625_add_color_blindness_and_other_columns
 */
class m210718_093625_add_color_blindness_and_other_columns extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{test_persons}}', 'color_blindness', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
