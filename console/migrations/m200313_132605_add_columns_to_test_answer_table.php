<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{test_answer}}`.
 */
class m200313_132605_add_columns_to_test_answer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->addColumn('test_answer', 'bww_rate', $this->integer());
		$this->addColumn('test_answer', 'dop_rate', $this->integer());
		$this->addColumn('test_answer', 'onc_rate', $this->integer());
		$this->addColumn('test_answer', 'onw_rate', $this->integer());
		$this->addColumn('test_answer', 'thr_rate', $this->integer());
		$this->addColumn('test_answer', 'img_prefix', $this->string(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		
    }
}
