<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{test_answer}}`.
 */
class m200209_184636_create_test_answer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{test_answer}}', [
            'id' => $this->primaryKey(),
			'test_person_id' => $this->integer(),
			'answer_number' => $this->integer(),
			'question_number' => $this->integer(),
			'answer_rate_1' => $this->string(3),
			'answer_rate_2' => $this->string(3),
			'answer_rate_3' => $this->string(3),
			'answer_rate_4' => $this->string(3),
			'answer_rate_5' => $this->string(3),
            'dop_stim_selected' => $this->integer(1)->defaultValue(0),
            'count_of_errors' => $this->integer()->defaultValue(0),
			'created_at' => $this->integer(),
        ]);
        
        $this->createTable('{{test_answer_dop_details}}', [
            'id' => $this->primaryKey(),
			'test_person_id' => $this->integer(),
            'test_answer_id' => $this->integer(),
			'answer_number' => $this->integer(),
			'question_number' => $this->integer(),
			'answer_name' => $this->string(8),
            'answer_rate' => $this->integer(),
            'img_prefix' => $this->string(1),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{test_answer}}');
        $this->dropTable('{{test_answer_dop_details}}');
    }
}
