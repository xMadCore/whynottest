<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{test_persons}}`.
 */
class m200112_234123_create_test_persons_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{test_persons}}', [
            'id' => $this->primaryKey(),
			
			'surname' => $this->string(),
			'age' => $this->integer(),
			'gender' => $this->integer(),
			'country' => $this->integer(),
			'level_of_education' => $this->integer(),
			'type_of_education' => $this->integer(),
			'is_art_education' => $this->integer(),
			'type_of_device' => $this->integer(),
			
			'array_of_questions_json' => $this->string(),
            'array_of_dop_stim_0_json' => $this->string(),
            'array_of_dop_stim_1_json' => $this->string(),
            'array_of_dop_stim_1_1_json' => $this->string(),
			'number_of_answer' => $this->integer()->defaultValue(1),
			
			'personal_token' => $this->string(32)->unique(),
			'auth_key' => $this->string(32)->notNull(),
			
			'created_at' => $this->integer(),
			'updated_at' => $this->integer(),
			'is_deleted' => $this->integer(1)->defaultValue(0),
			'deleted_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{test_persons}}');
    }
}
