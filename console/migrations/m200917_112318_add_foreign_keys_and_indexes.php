<?php

use yii\db\Migration;

/**
 * Class m200917_112318_add_foreign_keys_and_indexes
 */
class m200917_112318_add_foreign_keys_and_indexes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('idx-test_persons-age', '{{test_persons}}', '[[age]]');
        $this->createIndex('idx-test_persons-gender', '{{test_persons}}', '[[gender]]');
        $this->createIndex('idx-test_persons-country', '{{test_persons}}', '[[country]]');
        $this->createIndex('idx-test_persons-level_of_education', '{{test_persons}}', '[[level_of_education]]');
        $this->createIndex('idx-test_persons-type_of_education', '{{test_persons}}', '[[type_of_education]]');
        $this->createIndex('idx-test_persons-is_art_education', '{{test_persons}}', '[[is_art_education]]');
        $this->createIndex('idx-test_persons-type_of_device', '{{test_persons}}', '[[type_of_device]]');
        
        $this->createIndex('idx-test_persons-number_of_answer', '{{test_persons}}', '[[number_of_answer]]');
        
        $this->createIndex('idx-test_persons-created_at', '{{test_persons}}', '[[created_at]]');
        $this->createIndex('idx-test_persons-updated_at', '{{test_persons}}', '[[updated_at]]');
        $this->createIndex('idx-test_persons-is_deleted', '{{test_persons}}', '[[is_deleted]]');
        $this->createIndex('idx-test_persons-deleted_at', '{{test_persons}}', '[[deleted_at]]');
        
        $this->addForeignKey(
            'fk-test_answer-test_person_id',
            '{{test_answer}}',
            '[[test_person_id]]',
            '{{test_persons}}',
            '[[id]]',
            'CASCADE'
        );
        
        $this->createIndex('idx-test_persons-test_person_id', '{{test_answer}}', '[[test_person_id]]');
        $this->createIndex('idx-test_persons-answer_number', '{{test_answer}}', '[[answer_number]]');
        $this->createIndex('idx-test_persons-question_number', '{{test_answer}}', '[[question_number]]');
        $this->createIndex('idx-test_persons-answer_rate_1', '{{test_answer}}', '[[answer_rate_1]]');
        $this->createIndex('idx-test_persons-answer_rate_2', '{{test_answer}}', '[[answer_rate_2]]');
        $this->createIndex('idx-test_persons-answer_rate_3', '{{test_answer}}', '[[answer_rate_3]]');
        $this->createIndex('idx-test_persons-answer_rate_4', '{{test_answer}}', '[[answer_rate_4]]');
        $this->createIndex('idx-test_persons-answer_rate_5', '{{test_answer}}', '[[answer_rate_5]]');
        $this->createIndex('idx-test_persons-created_at', '{{test_answer}}', '[[created_at]]');
        
        $this->createIndex('idx-test_persons-bww_rate', '{{test_answer}}', '[[bww_rate]]');
        $this->createIndex('idx-test_persons-dop_rate', '{{test_answer}}', '[[dop_rate]]');
        $this->createIndex('idx-test_persons-onc_rate', '{{test_answer}}', '[[onc_rate]]');
        $this->createIndex('idx-test_persons-onw_rate', '{{test_answer}}', '[[onw_rate]]');
        $this->createIndex('idx-test_persons-thr_rate', '{{test_answer}}', '[[thr_rate]]');
        $this->createIndex('idx-test_persons-img_prefix', '{{test_answer}}', '[[img_prefix]]');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        
    }
}
