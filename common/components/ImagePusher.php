<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components;

use Yii;

/**
 * Description of imageРusher
 *
 * @author Antari
 */
class ImagePusher extends Configurable
{
    public $markerColor =       [0, 0, 0];
    public $leftToConvColor =   [255, 0, 0];
    public $rightToConvColor =  [0, 255, 0];
    public $topToConvColor =    [0, 0, 255];
    public $botToConvColor =    [0, 255, 255];
    const leftToConvMatrix =   [-1, 0];
    const rightToConvMatrix =  [1, 0];
    const topToConvMatrix =    [0, -1];
    const botToConvMatrix =    [0, 1];
    public $topLeftToConvColor = [150, 0, 0];
    public $topRightToConvColor = [0, 150, 0];
    public $botLeftToConvColor = [0, 0, 150];
    public $botRightToConvColor = [0, 150, 150];
    
    public $readedMask;
    public $maskConvergenceX;
    public $maskConvergenceY;
    public $maskRates;
    
    public $image; 
    
    protected static function matrix($array, $change) : array
    {
        foreach($change as $key => $value) {
            $array[$key] += $change[$key];
        }
        return $array;
    }
    
    protected static function matrixInverse($array) : array
    {
        foreach($array as $key => $value) {
            $array[$key] *= -1;
        }
        return $array;
    }
    
    protected static function matrixNegative($array) : array
    {
        foreach($array as $key => $value) {
            $array[$key] = $array[$key] > 0 ? $array[$key] * -1 : $array[$key];
        }
        return $array;
    }
    
    public function readMask($mask, $convergenceX = null, $convergenceY = null)
    {
        $this->readedMask = is_string($mask) ? imageCreateFromPNG($mask) : $mask;
        $maskWidth = imagesx($this->readedMask);
        $maskHeight = imagesy($this->readedMask);
        
        $this->maskConvergenceX = $convergenceX ?? $this->maskConvergenceX ?? intdiv($maskWidth, 2);
        $this->maskConvergenceY = $convergenceY ?? $this->maskConvergenceY ?? intdiv($maskHeight, 2);
        
        $markerColor = imagecolorallocate($this->readedMask, ...$this->markerColor);
        
        $x = 0;
        $y = 1;
        $maskWH = [$x => $maskWidth, $y => $maskHeight];
        $convergenceXY = [$x => $this->maskConvergenceX, $y => $this->maskConvergenceY];
        foreach (['->', '<-', 'v', '^'] as $direction) {
            $searcherXY = [$x => 0, $y => 0];
            $checkArray = [];
            if ($direction === '->') { //red
                $outer = $y;
                $inner = $x;
                $matrix = [-1, 0];
                $innerNext = 1;
                $innerStartPosition = 0;
                $lineColor = imagecolorallocate($this->readedMask, ...$this->leftToConvColor);
                
                $checkAngleArray = [
                    'end' => [[0,1], [-1,1], imagecolorallocate($this->readedMask, ...$this->topToConvColor), imagecolorallocate($this->readedMask, ...$this->topLeftToConvColor)],
                    'start' => [[0,-1], [-1,-1], imagecolorallocate($this->readedMask, ...$this->botToConvColor), imagecolorallocate($this->readedMask, ...$this->botLeftToConvColor)],
                ];
            }
            if ($direction === '<-') { //green
                $outer = $y;
                $inner = $x;
                $matrix = [1, 0];
                $innerNext = -1;
                $innerStartPosition = $maskWH[$inner]-1;
                $lineColor = imagecolorallocate($this->readedMask, ...$this->rightToConvColor);
                
                $checkAngleArray = [
                    'end' => [[0,1], [1,1], imagecolorallocate($this->readedMask, ...$this->topToConvColor), imagecolorallocate($this->readedMask, ...$this->topRightToConvColor)],
                    'start' => [[0,-1], [1,-1], imagecolorallocate($this->readedMask, ...$this->botToConvColor), imagecolorallocate($this->readedMask, ...$this->botRightToConvColor)],
                ];
                $anglePixelColor = imagecolorallocate($this->readedMask, ...$this->topRightToConvColor);
            }
            if ($direction === 'v') { //blue
                $outer = $x;
                $inner = $y;
                $matrix = [0, -1];
                $innerNext = 1;
                $innerStartPosition = 0;
                $lineColor = imagecolorallocate($this->readedMask, ...$this->topToConvColor);
                
                $checkAngleArray = [
                    'end' => [[1,0], [1,-1], imagecolorallocate($this->readedMask, ...$this->leftToConvColor), imagecolorallocate($this->readedMask, ...$this->topLeftToConvColor)],
                    'start' => [[-1,0], [-1,-1], imagecolorallocate($this->readedMask, ...$this->rightToConvColor), imagecolorallocate($this->readedMask, ...$this->topRightToConvColor)],
                ];
                $anglePixelColor = imagecolorallocate($this->readedMask, ...$this->botLeftToConvColor);
            }
            if ($direction === '^') { //more
                $outer = $x;
                $inner = $y;
                $matrix = [0, 1];
                $innerNext = -1;
                $innerStartPosition = $maskWH[$inner]-1;
                $lineColor = imagecolorallocate($this->readedMask, ...$this->botToConvColor);
                
                $checkAngleArray = [
                    'end' => [[1,0], [1,1], imagecolorallocate($this->readedMask, ...$this->leftToConvColor), imagecolorallocate($this->readedMask, ...$this->botLeftToConvColor)],
                    'start' => [[-1,0], [-1,1], imagecolorallocate($this->readedMask, ...$this->rightToConvColor), imagecolorallocate($this->readedMask, ...$this->botRightToConvColor)],
                ];
                $anglePixelColor = imagecolorallocate($this->readedMask, ...$this->botRightToConvColor);
            }
            
            for ($searcherXY[$outer] = 0; $searcherXY[$outer] < $maskWH[$outer]; $searcherXY[$outer]++) {
                $marker = false;
                for ($searcherXY[$inner] = $innerStartPosition; $searcherXY[$inner] !== ($convergenceXY[$inner]+$innerNext); $searcherXY[$inner]+=$innerNext) {
                    if ($marker === false && imagecolorat($this->readedMask, ...$searcherXY) === $markerColor) {
                        $marker = true;
                        continue;
                    }
                    
                    if ($marker === true && imagecolorat($this->readedMask, ...$searcherXY) !== $markerColor) {
                        $marker = false;
                        for ($deepenerXY = self::matrix($searcherXY, $matrix); $deepenerXY[$outer] < $maskHeight; $deepenerXY[$outer]++) {
                            if (imagecolorat($this->readedMask, ...$deepenerXY) !== $markerColor || imagecolorat($this->readedMask, ...self::matrix($deepenerXY, self::matrixInverse($matrix))) === $markerColor) {
                                if (!key_exists($deepenerXY[$x]. '.' .$deepenerXY[$y], $checkArray)) {
                                    $checkArray[$deepenerXY[$x]. '.' .$deepenerXY[$y]] = null;
                                    imageline($this->readedMask, ...$searcherXY, ...$lastPixel = self::matrix($deepenerXY, self::matrix(self::matrixNegative(array_reverse($matrix)), self::matrixInverse($matrix))), ...[$lineColor]);
                                    
                                    if (imagecolorat($this->readedMask, ...self::matrix($lastPixel, $checkAngleArray['end'][0])) !== $markerColor && imagecolorat($this->readedMask, ...self::matrix($lastPixel, $checkAngleArray['end'][1])) === $checkAngleArray['end'][2]) {
                                        imageline($this->readedMask, ...self::matrix($lastPixel, $checkAngleArray['end'][0]), ...self::matrix($lastPixel, $checkAngleArray['end'][0]), ...[$checkAngleArray['end'][3]]);
                                    }
                                    if (imagecolorat($this->readedMask, ...self::matrix($searcherXY, $checkAngleArray['start'][0])) !== $markerColor && imagecolorat($this->readedMask, ...self::matrix($searcherXY, $checkAngleArray['start'][1])) === $checkAngleArray['start'][2]) {
                                        imageline($this->readedMask, ...self::matrix($searcherXY, $checkAngleArray['start'][0]), ...self::matrix($searcherXY, $checkAngleArray['start'][0]), ...[$checkAngleArray['start'][3]]);
                                    }
                                }
                                break;
                            }
                        }
                        continue;
                    }
                }
            }
        }
        //imagepng($this->readedMask, Yii::getAlias('@frontend/web/images/mask_prerender.png'));
        return $this->readedMask;
    }

    public static function colorMutator(array $rgb, $version)
    {
        switch ($version) {
            case '1':
                foreach($rgb as $key => $color) {
                    $rgb[$key] = $color <= 25 ? 25 : $color - 25;
                }
                break;
            case '2';
                foreach($rgb as $key => $color) {
                    $rgb[$key] = $color <= 50 ? 50 : $color - 50;
                }
                break;
        }
        return $rgb;
    }
    
    protected static function getBufferImage($maskWidth, $maskHeight)
    {
        $bufferBufferImage = imageCreateTruecolor($maskWidth, $maskHeight);
        imageSaveAlpha($bufferBufferImage, true);
        $transparent = imageColorAllocateAlpha($bufferBufferImage, 255, 255, 255, 127);
        imageFill($bufferBufferImage, 0, 0, $transparent);
        return $bufferBufferImage;
    }
    
    protected function renderLine(&$bufferBuffer, &$strongBuffer, $x, $y, $desiredColor, $startX_image, $startY_image, $matrix, $version, &$b3uffer = null)
    {
        $maskWidth = imagesx($this->readedMask);
        $maskHeight = imagesy($this->readedMask);
        $desiredColor = is_array($desiredColor) ? imagecolorallocate($this->readedMask, ...$desiredColor) : $desiredColor;
        $markerColor = imagecolorallocate($this->readedMask, ...$this->markerColor);
        $halfRed = imagecolorallocate($this->readedMask, ...$this->topLeftToConvColor);
        $halfGreen = imagecolorallocate($this->readedMask, ...$this->topRightToConvColor);
        $halfBlue = imagecolorallocate($this->readedMask, ...$this->botLeftToConvColor);
        $halfLightGreen = imagecolorallocate($this->readedMask, ...$this->botRightToConvColor);
        
        $pixelColor = imagecolorat($this->readedMask, $x, $y);
        if (in_array($pixelColor, [$desiredColor, $halfRed, $halfGreen, $halfBlue, $halfLightGreen]) ) {
            $bufferBuffer = $bufferBuffer ? : self::getBufferImage($maskWidth, $maskHeight);
            $matrix = $pixelColor === $halfRed ? [-1, -1] : $matrix;
            $matrix = $pixelColor === $halfGreen ? [1, -1] : $matrix;
            $matrix = $pixelColor === $halfBlue ? [-1, 1] : $matrix;
            $matrix = $pixelColor === $halfLightGreen ? [1, 1] : $matrix;
            $indexColor = imagecolorat($this->image, ...self::matrix([$startX_image + $x, $startY_image + $y], $matrix));
            $lineColor = $this->colorMutator([($indexColor >> 16) & 0xFF, ($indexColor >> 8) & 0xFF, $indexColor & 0xFF], $version);
            $lineColor = imagecolorallocate($bufferBuffer, ...$lineColor);
            imageline($bufferBuffer, $x, $y, $this->maskConvergenceX, $this->maskConvergenceY, $lineColor);
        }
        if ($pixelColor === $markerColor) {
            $lineColor = imagecolorat($this->image, $startX_image + $x, $startY_image + $y);
            imagesetpixel($strongBuffer, $x, $y, $lineColor);
        }
        if ($pixelColor === imagecolorallocate($this->readedMask, ...[255,255,255])) {
            $lineColor = imagecolorat($this->image, $startX_image + $x, $startY_image + $y);
            imagesetpixel($b3uffer, $x, $y, $lineColor);
        }
    }
    
    public function executeMask($startX_image, $startY_image, $image = null, $multiplier = 0.5, $maskPrerender = null, $convergenceX_mask = null, $convergenceY_mask = null)
    {
        $this->image = is_string($image) ? imageCreateFromPNG($image) : $image ?? $this->image;
        $this->readedMask = is_string($maskPrerender) ? imageCreateFromPNG($maskPrerender) : $maskPrerender ?? $this->readedMask;
        
        $maskWidth = imagesx($this->readedMask);
        $maskHeight = imagesy($this->readedMask);
        
        $this->maskConvergenceX = $convergenceX_mask ?? $this->maskConvergenceX ?? intdiv($maskWidth, 2);
        $this->maskConvergenceY = $convergenceY_mask ?? $this->maskConvergenceY ?? intdiv($maskHeight, 2);
        
        $rate = [
            '^' => $this->maskConvergenceY,
            'v' => $maskHeight-1 - $this->maskConvergenceY,
            '->' => $maskWidth-1 - $this->maskConvergenceX,
            '<-' => $this->maskConvergenceX
        ];
        
        $maxIterations = [
            '^' => $maskHeight - $this->maskConvergenceY - 1,
            'v' => $this->maskConvergenceY,
            '->' => $this->maskConvergenceX,
            '<-' => $maskWidth - $this->maskConvergenceX - 1
        ];
        
        $markerColor = imagecolorallocate($this->readedMask, ...$this->markerColor);
        
        $bufferImage = self::getBufferImage($maskWidth, $maskHeight);
        $strongBuffer = self::getBufferImage($maskWidth, $maskHeight);
        $b3uffer = self::getBufferImage($maskWidth, $maskHeight);
        
        $iterator = new ImageSideIterator(['rate' => $rate, 'maxIterations' => $maskWidth+$maskHeight+2, 'eachMaxIterations' => $maxIterations]);
        foreach($iterator as $key => $coordinate) {
            switch ($key) {
                case 'v':
                    $bufferBufferImage = null;
                    for ($x = 0, $y = $coordinate; $x < $maskWidth; $x++) {
                        $this->renderLine($bufferBufferImage, $strongBuffer, $x, $y, $this->topToConvColor, $startX_image, $startY_image, self::topToConvMatrix, 1, $b3uffer);
                    }
                    if ($bufferBufferImage !== null) {
                        imagealphablending($bufferBufferImage, false);
                        imagefilledrectangle($bufferBufferImage, 0, $this->maskConvergenceY - (int) abs(($this->maskConvergenceY - $y) * $multiplier), $maskWidth - 1, $maskHeight, imageColorAllocateAlpha($bufferBufferImage, 255, 255, 255, 127));
                        imagealphablending($bufferBufferImage, true);
                        imagecopy($bufferImage, $bufferBufferImage, 0, 0, 0, 0, $maskWidth, $maskHeight);
                        imagedestroy($bufferBufferImage);
                    }
                    break;
                case '^':
                    $bufferBufferImage = null;
                    
                    for ($x = 0, $y = $maskHeight - $coordinate - 1; $x < $maskWidth; $x++) {
                        $this->renderLine($bufferBufferImage, $strongBuffer, $x, $y, $this->botToConvColor, $startX_image, $startY_image, self::botToConvMatrix, 1, $b3uffer);
                    }
                    if ($bufferBufferImage !== null) {
                        imagealphablending($bufferBufferImage, false);
                        imagefilledrectangle($bufferBufferImage, 0, $this->maskConvergenceY + (int) abs(($this->maskConvergenceY - $y) * $multiplier), $maskWidth - 1, 0, imageColorAllocateAlpha($bufferBufferImage, 255, 255, 255, 127));
                        imagealphablending($bufferBufferImage, true);
                        imagecopy($bufferImage, $bufferBufferImage, 0, 0, 0, 0, $maskWidth, $maskHeight);
                        imagedestroy($bufferBufferImage);
                    }
                    break;
                case '->':
                    $bufferBufferImage = null;
                    
                    for ($x = $coordinate, $y = 0; $y < $maskHeight; $y++) {
                        $this->renderLine($bufferBufferImage, $strongBuffer, $x, $y, $this->leftToConvColor, $startX_image, $startY_image, self::leftToConvMatrix, 2, $b3uffer);
                    }
                    if ($bufferBufferImage !== null) {
                        imagealphablending($bufferBufferImage, false);
                        imagefilledrectangle($bufferBufferImage, $this->maskConvergenceX - (int) abs(($this->maskConvergenceX - $x) * $multiplier), 0, $maskWidth, $maskHeight - 1, imageColorAllocateAlpha($bufferBufferImage, 255, 255, 255, 127));
                        imagealphablending($bufferBufferImage, true);
                        imagecopy($bufferImage, $bufferBufferImage, 0, 0, 0, 0, $maskWidth, $maskHeight);
                        imagedestroy($bufferBufferImage);
                    }
                    break;
                case '<-':
                    $bufferBufferImage = null;
                    
                    for ($x = $maskWidth - $coordinate - 1, $y = 0; $y < $maskHeight; $y++) {
                        $this->renderLine($bufferBufferImage, $strongBuffer, $x, $y, $this->rightToConvColor, $startX_image, $startY_image, self::rightToConvMatrix, 2, $b3uffer);
                    }
                    if ($bufferBufferImage !== null) {
                        imagealphablending($bufferBufferImage, false);
                        imagefilledrectangle($bufferBufferImage, $this->maskConvergenceX + (int) abs(($this->maskConvergenceX - $x) * $multiplier), 0, 0, $maskHeight - 1, imageColorAllocateAlpha($bufferBufferImage, 255, 255, 255, 127));
                        imagealphablending($bufferBufferImage, true);
                        imagecopy($bufferImage, $bufferBufferImage, 0, 0, 0, 0, $maskWidth, $maskHeight);
                        imagedestroy($bufferBufferImage);
                    }
                    break;
            }
        }
        
        $bufferBufferImage = imageCreateTruecolor($maskWidth, $maskHeight);
        imageSaveAlpha($bufferBufferImage, true);
        $transparent = imageColorAllocateAlpha($bufferBufferImage, 0, 0, 0, 30);
        imageFill($bufferBufferImage, 0, 0, $transparent);
        
        imageSaveAlpha($b3uffer, true);
//        imagecopy($this->image, $b3uffer, (int) abs(($this->maskConvergenceX - 0) * $multiplier)-150, (int) abs(($this->maskConvergenceY - 0) * $multiplier)-150, 0, 0, (int) $maskWidth * $multiplier, (int) $maskHeight * $multiplier);
        imagecopy($this->image, $bufferBufferImage, $startX_image, $startY_image, 0, 0, $maskWidth, $maskHeight);
        imagecopy($this->image, $bufferImage, $startX_image, $startY_image, 0, 0, $maskWidth, $maskHeight);
        imagecopy($this->image, $strongBuffer, $startX_image, $startY_image, 0, 0, $maskWidth, $maskHeight);
        
        return $this->image;
    }
}
