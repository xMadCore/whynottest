<?php

namespace common\components;

use Yii;
use yii\base\Widget;
use yii\bootstrap4\Html;
use yii\web\View;

/**
 * Description of JsBufferWidget
 *
 * @author Antari
 */
class JsBufferWidget extends Widget
{
    public $forPjax = true;
    
    public $jsArrayCountMarker;
//	public $scriptMarker;
	public $cutAfterBuffering = true;
	public $viewJsBuffer;
	
//	public $activeFormAttributesCounter;
//	public $activeFormAttributesBuffer;
	
	public function init()
    {
        parent::init();
        $this->getView()->registerJs(';;');
        $this->jsArrayCountMarker = count($this->getView()->js[View::POS_READY]);
                
//		$this->scriptMarker = $this->scriptMarker ?? ';' . $this->getId() . '=0;';
//		$this->getView()->registerJs($this->scriptMarker, View::POS_READY, $this->scriptMarker);
		
		//$this->activeFormAttributesCounter = count($this->form->attributes);
    }

    public function run()
    {
        if (!$this->forPjax || Yii::$app->request->isPjax) {
            $this->viewJsBuffer = array_slice($this->getView()->js[View::POS_READY], $this->jsArrayCountMarker);
            
            if ($this->cutAfterBuffering) {
                array_splice($this->getView()->js[View::POS_READY], $this->jsArrayCountMarker);
            }

            return Html::script(implode("\n", $this->viewJsBuffer));
            //return "<script>jQuery(function ($) {" . implode("\n", $this->viewJsBuffer) . "});</script>";
        }
        return '';
    }
}