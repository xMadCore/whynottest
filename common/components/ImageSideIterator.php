<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components;

/**
 * Description of ImageSideIterator
 *
 * @author Antari
 */

class ImageSideIterator extends Configurable implements \Iterator
{
    protected $position = 1;
    protected $maxIterations = 0;
    protected $rate = [];
    protected $value = [];
    protected $eachPosition = [];
    protected $eachMaxIterations = [];

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        $this->position = 1;
        foreach($this->rate as $key => $value) {
            $this->rate[$key] = $value > 0 ? $value : 1;
            $this->value[$key] = 0;
            $this->eachPosition[$key] = 0;
        }
    }
    
    /**
     * {@inheritdoc}
     */
    public function rewind()
    {
        $this->init();
    }

    /**
     * {@inheritdoc}
     */
    public function current()
    {
        return $this->eachPosition[$this->key()];
    }

    /**
     * {@inheritdoc}
     */
    public function key()
    {
        return array_search(min($this->value), $this->value);
    }

    /**
     * {@inheritdoc}
     */
    public function next()
    {
        $k = $this->key();
        if (isset($this->eachMaxIterations[$k]) && $this->eachPosition[$k] >= $this->eachMaxIterations[$k]) {
            $k = $this->key();
            unset($this->eachPosition[$k]);
            unset($this->rate[$k]);
            unset($this->value[$k]);
            unset($this->eachMaxIterations[$k]);
        } else {
            ++$this->eachPosition[$k];
            $this->value[$k]+=$this->rate[$k];
        }
        ++$this->position;
    }

    /**
     * {@inheritdoc}
     */
    public function valid()
    {
        return $this->position <= $this->maxIterations && !empty($this->eachPosition);
    }
}
