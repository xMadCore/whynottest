<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components;

use Yii;

/**
 * Description of BsImg
 *
 * @author Antari
 */
class BsImg
{
    public static $sourcePath = '@npm/bootstrap-icons/icons/';
    public static $noFileReturn = '<svg width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"></svg>';
    
    public static function getSvg($name) : string
    {
        $svgName = Yii::getAlias(static::$sourcePath . $name) . '.svg';
        return file_exists($svgName) ? file_get_contents($svgName) : static::$noFileReturn;
    }
}
