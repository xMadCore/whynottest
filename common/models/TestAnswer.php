<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "test_answer".
 *
 * @property int $id
 * @property int|null $test_person_id
 * @property int|null $answer_number
 * @property int|null $question_number
 * @property string|null $answer_rate_1
 * @property string|null $answer_rate_2
 * @property string|null $answer_rate_3
 * @property string|null $answer_rate_4
 * @property string|null $answer_rate_5
 * @property int|null $created_at
 * @property int|null $bww_rate
 * @property int|null $dop_rate
 * @property int|null $onc_rate
 * @property int|null $onw_rate
 * @property int|null $thr_rate
 * @property string|null $img_prefix
 *
 * @property TestPersons $testPerson
 */
class TestAnswer extends ActiveRecord
{
	public $stringAnswers;
	
	/**
     * {@inheritdoc}
     */
	public static function tableName()
    {
        return '{{test_answer}}';
    }
	
	/**
     * {@inheritdoc}
     */
	public function rules()
    {
        $typesImplode = implode('|', static::getTypesArray());
        return [
			//['stringAnswers', 'match', 'pattern' => '/^((' . implode('|', static::getTypesArray()) . '):){5}$/'],
            ['stringAnswers', 'match', 'pattern' => "/(($typesImplode):.*){5}/"],
			['stringAnswers', 'required'],
        ];
    }
	
	public static function getTypesArray()
	{
		return [
			'bww',
			'dop',
			'onc',
			'onw',
			'thr',
		];
	}
    
    public static function getTypes0Array()
	{
		return [
			'bww',
			'onc',
			'onw',
		];
	}
    
    public static function getTypes1Array()
	{
		return [
			'bww',
			'dop',
			'onc',
			'onw',
		];
	}
	
	public function beforeSave($insert)
	{
		if ($insert) {
			$this->created_at = time();
            
            $arrayAllAnswers = explode(':', $this->stringAnswers);
            $arrayAnswers = array_filter($arrayAllAnswers, function ($value) {
                return in_array($value, $this->getTypesArray());
            });
            $arrayAnswers = array_values($arrayAnswers);
            
            for ($i = 0; $i < Yii::$app->params['numberOfTypesOfAnswer']; $i++) {
                $attribute = 'answer_rate_' . ($i+1);
                $this->$attribute = $arrayAnswers[$i];
            }
            
            for ($i = 1; $i <= Yii::$app->params['numberOfTypesOfAnswer']; $i++) {
                $attribute = 'answer_rate_' . $i;
                $this[$this->$attribute . '_rate'] = $i;
            }
            
            if (count($arrayAllAnswers)-1 > Yii::$app->params['numberOfTypesOfAnswer']) {
                $this->dop_stim_selected = 1;
                $this->count_of_errors = count($arrayAllAnswers) - Yii::$app->params['numberOfTypesOfAnswer'];
                foreach($arrayAllAnswers as $key => $answerName) {
                    if (!$answerName) {
                        continue;
                    }
                    if (in_array($answerName, $this->getTypesArray())) {
                        $answerName = "t{$this->img_prefix}" . str_pad($this->question_number, 2, '0', STR_PAD_LEFT) . "{$answerName}2";
                    }

                    (new TestAnswerDopDetail([
                        'test_person_id' => $this->test_person_id,
                        //'test_answer_id' => $this->test_answer_id,
                        'answer_number' => $this->answer_number,
                        'question_number' => $this->question_number,
                        'answer_name' => $answerName,
                        'answer_rate' => $key + 1,
                        'img_prefix' => $this->img_prefix,
                    ]))->save(false);
                }
            }
		}
		return parent::beforeSave($insert);
	}
    
    public function afterSave($insert, $changedAttributes)
    {
        $details = TestAnswerDopDetail::find()->where([
            'test_person_id' => $this->test_person_id,
            'answer_number' => $this->answer_number,
            'question_number' => $this->question_number,
            'img_prefix' => $this->img_prefix,
            'test_answer_id' => null,
        ])->all();
        foreach($details as $detail) {
            $detail->test_answer_id = $this->id;
            $detail->save(false);
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * Gets query for [[TestPerson]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTestPerson()
    {
        return $this->hasOne(TestPersons::class, ['id' => 'test_person_id'])->inverseOf('testAnswers');
    }
    
}
