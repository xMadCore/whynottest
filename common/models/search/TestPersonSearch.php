<?php

namespace common\models\search;

use yii\data\ActiveDataProvider;
use common\models\TestPerson;

/**
 * TestPersonSearch represents the model behind the search form of `common\models\TestPerson`.
 */
class TestPersonSearch extends TestPerson
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'age',
                    'gender',
                    'country',
                    'level_of_education',
                    'type_of_education',
                    'is_art_education',
                    'type_of_device',
                    'number_of_answer',
                    'created_at',
                    'updated_at',
                    'is_deleted',
                    'deleted_at'
                ],
                'integer'
            ],
            [['surname', 'array_of_questions_json', 'personal_token', 'auth_key'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TestPerson::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'age' => $this->age,
            'gender' => $this->gender,
            'country' => $this->country,
            'level_of_education' => $this->level_of_education,
            'type_of_education' => $this->type_of_education,
            'is_art_education' => $this->is_art_education,
            'type_of_device' => $this->type_of_device,
            'number_of_answer' => $this->number_of_answer,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'is_deleted' => $this->is_deleted,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'surname', $this->surname])
            ->andFilterWhere(['like', 'array_of_questions_json', $this->array_of_questions_json])
            ->andFilterWhere(['like', 'personal_token', $this->personal_token])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key]);

        return $dataProvider;
    }
}
