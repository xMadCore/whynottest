<?php

namespace common\models\search;

use yii\data\ActiveDataProvider;
use common\models\TestAnswerDopDetail;

/**
 * TestAnswerSearch represents the model behind the search form of `common\models\TestPerson`.
 */
class TestAnswerDetailSearch extends TestAnswerDopDetail
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'test_person_id',
                    'test_answer_id',
                    'answer_number',
                    'question_number',
                ],
                'integer'
            ],
            [['img_prefix'], 'string'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TestAnswerDopDetail::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'test_person_id' => $this->test_person_id,
            'test_answer_id' => $this->test_answer_id,
            'answer_number' => $this->answer_number,
            'question_number' => $this->question_number,
        ]);

        $query
            ->andFilterWhere(['like', 'img_prefix', $this->img_prefix]);

        return $dataProvider;
    }
}
