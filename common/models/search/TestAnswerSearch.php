<?php

namespace common\models\search;

use yii\data\ActiveDataProvider;
use common\models\TestAnswer;

/**
 * TestAnswerSearch represents the model behind the search form of `common\models\TestPerson`.
 */
class TestAnswerSearch extends TestAnswer
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'test_person_id',
                    'answer_number',
                    'question_number',
                    'created_at',
                    'bww_rate',
                    'dop_rate',
                    'onc_rate',
                    'onw_rate',
                    'thr_rate',
                ],
                'integer'
            ],
            [['answer_rate_1', 'answer_rate_2', 'answer_rate_3', 'answer_rate_4', 'answer_rate_5', 'img_prefix'], 'string'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TestAnswer::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'test_person_id' => $this->test_person_id,
            'answer_number' => $this->answer_number,
            'question_number' => $this->question_number,
            'bww_rate' => $this->bww_rate,
            'dop_rate' => $this->dop_rate,
            'onc_rate' => $this->onc_rate,
            'onw_rate' => $this->onw_rate,
            'thr_rate' => $this->thr_rate,
        ]);

        $query->andFilterWhere(['like', 'answer_rate_1', $this->img_prefix])
            ->andFilterWhere(['like', 'answer_rate_2', $this->img_prefix])
            ->andFilterWhere(['like', 'answer_rate_3', $this->img_prefix])
            ->andFilterWhere(['like', 'answer_rate_4', $this->img_prefix])
            ->andFilterWhere(['like', 'answer_rate_5', $this->img_prefix])
            ->andFilterWhere(['like', 'img_prefix', $this->img_prefix]);

        return $dataProvider;
    }
}
