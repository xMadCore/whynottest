<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\behaviors\TimestampBehavior;

/**
 * @property int $id
 * @property string|null $surname
 * @property int|null $age
 * @property int|null $gender
 * @property int|null $country
 * @property int|null $level_of_education
 * @property int|null $type_of_education
 * @property int|null $is_art_education
 * @property int|null $type_of_device
 * @property string|null $array_of_questions_json
 * @property string|null $array_of_dop_stim_0_json
 * @property string|null $array_of_dop_stim_1_json
 * @property string|null $array_of_dop_stim_1_1_json
 * @property int|null $number_of_answer
 * @property string|null $personal_token
 * @property string $auth_key
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $is_deleted
 * @property int|null $deleted_at
 
 * @property TestAnswer[] $testAnswers
 * 
 * @property-read array $arrayOfQuestions
 */
class TestPerson extends ActiveRecord implements IdentityInterface
{
    const AGE_00_13 = 1;
    const AGE_14_17 = 2;
    const AGE_18_24 = 3;
    const AGE_25_34 = 4;
    const AGE_35_44 = 5;
    const AGE_45_59 = 6;
    const AGE_60_00 = 7;
    
	const GENDER_MALE = 0;
	const GENDER_FEMALE = 1;
    const GENDER_OTHER = 10;
	
	const COUNTRY_RUSSIA = 7;
	const COUNTRY_USA = 1;
	const COUNTRY_UK = 44;
	const COUNTRY_GERMANY = 49;
	const COUNTRY_SPAIN = 34;
	const COUNTRY_ITALY = 39;
	const COUNTRY_CHINA = 86;
	const COUNTRY_KOREA = 82;
	const COUNTRY_FINLAND = 358;
	const COUNTRY_FRANCE = 33;
	const COUNTRY_JAPAN = 81;
	const COUNTRY_OTHER = -1;
    
    const COLOR_BLIND_NOT = 0;
    const COLOR_BLIND_GREEN_RED = 1;
    const COLOR_BLIND_BLUE_GREEN = 2;
	
	const LEVEL_WITHOUT = 0;
	const LEVEL_SECONDARY = 2;
	const LEVEL_INCOMPLETE_HIGHER = 4;
	const LEVEL_HIGHER = 6;
	
	const TYPE_WITHOUT = 0;
	const TYPE_TECHNICAL = 1;
	const TYPE_HUMANITARIAN = 3;
	
	const DEVICE_PC = 1;
	const DEVICE_TABLET = 2;
	const DEVICE_SMART = 3;

	/**
     * {@inheritdoc}
     */
	public static function tableName()
    {
        return '{{test_persons}}';
    }
	
	/**
     * {@inheritdoc}
     */
	public function rules()
    {
        return [
			['surname', 'string', 'min' => 2, 'max' => 100],
			//['age', 'number','min' => 8, 'max' => 108],
            ['age', 'in', 'range' => array_keys(static::getAgesArray())],
			[
                [
                    'gender',
                    'country',
                    'level_of_education',
                    'type_of_education',
                    'is_art_education',
                    'type_of_device',
                ], 
                'integer'
            ],
			['gender', 'in', 'range' => array_keys(static::getGendersArray())],
			['country', 'in', 'range' => array_keys(static::getCountriesArray())],
            ['color_blindness', 'in', 'range' => array_keys(static::getColorBlindnessArray())],
			['level_of_education', 'in', 'range' => array_keys(static::getLevelsOfEducationArray())],
			['type_of_education', 'in', 'range' => array_keys(static::getTypesOfEducationArray())],
			['is_art_education', 'in', 'range' => array_keys(static::getYesNoArray())],
			['type_of_device', 'in', 'range' => array_keys(static::getDevicesArray())],
			[
                [
                    'surname',
                    'age',
                    'gender',
                    'country',
                    'level_of_education',
                    'type_of_education',
                    'is_art_education',
                    'type_of_device',
                ],
                'required'
            ],
        ];
    }
	
	/**
     * {@inheritdoc}
     */
	public function attributeLabels()
	{
		return [
			'surname' => Yii::t('frontend', 'Surname or Nickname'),
			'age' => Yii::t('frontend', 'Age'),
			'gender' => Yii::t('frontend', 'Gender'),
			'country' => Yii::t('frontend', 'Country'),
            'color_blindness' => Yii::t('frontend', 'Color blindness'),
			'level_of_education' => Yii::t('frontend', 'The level of Education'),
			'type_of_education' => Yii::t('frontend', 'Type of Education'),
			'is_art_education' => Yii::t('frontend', 'The presence of art Education'),
			'type_of_device' => Yii::t('frontend', 'Type of device you are testing from'),
		];
	}
    
    public function behaviors()
	{
		return [
            ['class' => TimestampBehavior::class],
		];
	}
	
    public static function getAgesArray() : array
	{
		return [
			self::AGE_00_13 => Yii::t('frontend', '< 14'),
			self::AGE_14_17 => Yii::t('frontend', '14 - 17'),
            self::AGE_18_24 => Yii::t('frontend', '18 - 24'),
            self::AGE_25_34 => Yii::t('frontend', '25 - 34'),
            self::AGE_35_44 => Yii::t('frontend', '35 - 44'),
			self::AGE_45_59 => Yii::t('frontend', '45 - 59'),
            self::AGE_60_00 => Yii::t('frontend', '60+'),
		];
	}
    
    public function getAgeString()
	{
		return $this->age > 7 ? $this->age . ' (old)' : $this->getAgesArray()[$this->age];
	}
    
	public static function getGendersArray() : array
	{
		return [
			self::GENDER_MALE => Yii::t('frontend', 'Male'),
			self::GENDER_FEMALE => Yii::t('frontend', 'Female'),
            self::GENDER_OTHER => Yii::t('frontend', 'Other'),
		];
	}
	
	public static function getCountriesArray() : array
	{
		return [
			self::COUNTRY_RUSSIA => Yii::t('frontend', 'Russia'),
			self::COUNTRY_USA => Yii::t('frontend', 'USA'),
			self::COUNTRY_UK => Yii::t('frontend', 'United Kingdom'),
			self::COUNTRY_GERMANY => Yii::t('frontend', 'Germany'),
			self::COUNTRY_SPAIN => Yii::t('frontend', 'Spain'),
			self::COUNTRY_ITALY => Yii::t('frontend', 'Italy'),
			self::COUNTRY_CHINA => Yii::t('frontend', 'China'),
			self::COUNTRY_KOREA => Yii::t('frontend', 'Korea'),
			self::COUNTRY_FINLAND => Yii::t('frontend', 'Finland'),
			self::COUNTRY_FRANCE => Yii::t('frontend', 'France'),
			self::COUNTRY_JAPAN => Yii::t('frontend', 'Japan'),
			self::COUNTRY_OTHER => Yii::t('frontend', 'other'),
		];
	}
    
    public static function getColorBlindnessArray() : array
    {
        return [
            self::COLOR_BLIND_NOT => Yii::t('frontend', 'No'),
            self::COLOR_BLIND_GREEN_RED => Yii::t('frontend', 'Green - Red'),
            self::COLOR_BLIND_BLUE_GREEN => Yii::t('frontend', 'Blue - Green'),
        ];
    }
	
	public static function getLevelsOfEducationArray() : array
	{
		return [
			self::LEVEL_WITHOUT => Yii::t('frontend', 'Without education'),
			self::LEVEL_SECONDARY => Yii::t('frontend', 'Secondary education'),
			self::LEVEL_INCOMPLETE_HIGHER => Yii::t('frontend', 'Unfinished higher education'),
			self::LEVEL_HIGHER => Yii::t('frontend', 'Higher education'),
		];
	}
	
	public static function getTypesOfEducationArray() : array
	{
		return [
			self::TYPE_WITHOUT => \Yii::t('frontend', 'Without type'),
			self::TYPE_TECHNICAL => \Yii::t('frontend', 'Technical education'),
			self::TYPE_HUMANITARIAN => \Yii::t('frontend', 'Humanitarian education'),
		];
	}
	
	public static function getYesNoArray() : array
	{
		return [
			1 => \Yii::t('frontend', 'Yes'),
			0 => \Yii::t('frontend', 'No'),
		];
	}

	public static function getDevicesArray() : array
	{
		return [
			self::DEVICE_PC => \Yii::t('frontend', 'Personal Computer'),
			self::DEVICE_TABLET => \Yii::t('frontend', 'Tablet'),
			self::DEVICE_SMART => \Yii::t('frontend', 'Smartphone'),
		];
	}
    
    /**
     * {@inheritdoc}
     */
	public function beforeSave($insert)
	{
        $isAnime = Yii::$app->params['testVersion'] === 'anime';
        $numberOfQuestions = Yii::$app->params['numberOfQuestions'];
		if ($insert) {
            $this->color_blindness = $this->color_blindness ?: self::COLOR_BLIND_NOT;
            
			$this->generateAuthKey();
			$this->personal_token = Yii::$app->security->generateRandomString(32);
			
			$arrayOfQuestions = range(1, $numberOfQuestions);
			shuffle($arrayOfQuestions);
			$this->array_of_questions_json = json_encode(array_merge([0], $arrayOfQuestions));
            
            if ($isAnime) {
                $arrayOfDopStim0 = range(1, 26);
            } else {
                $arrayOfDopStim0 = range(1, 25);
            }
			shuffle($arrayOfDopStim0);
			$this->array_of_dop_stim_0_json = json_encode(array_merge([0], $arrayOfDopStim0));
            
            $arrayOfDopStim1 = range(1, 25);
			shuffle($arrayOfDopStim1);
			$this->array_of_dop_stim_1_json = json_encode(array_merge([0], $arrayOfDopStim1));
            
            if ($isAnime) {
                $arrayOfDopStim11 = range(26, 54);
            } else {
                $arrayOfDopStim11 = range(26, 54);
                unset($arrayOfDopStim11['21']);
                $arrayOfDopStim11 = array_values($arrayOfDopStim11);
            }
			shuffle($arrayOfDopStim11);
			$this->array_of_dop_stim_1_1_json = json_encode(array_merge([0], $arrayOfDopStim11));
		}
		return parent::beforeSave($insert);
	}
    
    /**
     * Gets query for [[TestAnswer]].
     *
     * @return \yii\db\ActiveQuery
     */
	public function getTestAnswers()
    {
        return $this->hasMany(TestAnswer::class, ['test_person_id' => 'id'])->inverseOf('testPerson');
    }
    
    /**
     * @return integer[]
     */
	public function getArrayOfQuestions() : array
	{
		return (array) json_decode($this->array_of_questions_json, true);
	}
    
    public function getArrayOfDopStim0() : array
	{
		return (array) json_decode($this->array_of_dop_stim_0_json, true);
	}
    
    public function getArrayOfDopStim1() : array
	{
		return (array) json_decode($this->array_of_dop_stim_1_json, true);
	}
    
    public function getArrayOfDopStim11() : array
	{
		return (array) json_decode($this->array_of_dop_stim_1_1_json, true);
	}
    
    public function getIsTestFinished() : bool
    {
        return $this->number_of_answer > Yii::$app->params['numberOfQuestions'];
    }
    
	/**
     * {@inheritdoc}
     * @see IdentityInterface
     */
	public static function findIdentity($id)
	{
		return static::findOne(['id' => $id, 'is_deleted' => 0]);
	}
	
	/**
     * {@inheritdoc}
     * @see IdentityInterface
     */
	public static function findIdentityByAccessToken($token, $type = null)
	{
		return static::findOne(['personal_token' => $token, 'is_deleted' => 0]);
	}
	
	/**
     * {@inheritdoc}
     * @see IdentityInterface
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     * @see IdentityInterface
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }
	
	/**
     * {@inheritdoc}
     * @see IdentityInterface
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
	
	/**
     * {@inheritdoc}
     * @see IdentityInterface
     */
	public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
}
