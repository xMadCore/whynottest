<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "test_answer".
 *
 * @property int $id
 * @property int|null $test_person_id
 * @property int|null $answer_number
 * @property int|null $question_number
 * @property string|null $answer_name
 * @property int|null $answer_rate
 * @property int|null $created_at
 * @property string|null $img_prefix
 *
 * @property TestPersons $testPerson
 */
class TestAnswerDopDetail extends ActiveRecord
{
	/**
     * {@inheritdoc}
     */
	public static function tableName()
    {
        return '{{test_answer_dop_details}}';
    }

    /**
     * Gets query for [[TestPerson]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTestPerson()
    {
        return $this->hasOne(TestPersons::class, ['id' => 'test_person_id'])->inverseOf('testAnswers');
    }
}
