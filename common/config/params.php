<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
    'bsVersion' => '4.x',
    'numberOfQuestions' => 25,
    'numberOfTypesOfAnswer' => 5,
    'testVersion' => 'photo',
//    'testVersion' => 'anime',
    'link.anime' => 'https://whynottest.cookiereaper.ru/en/site/index?action=1',
    'link.photo' => 'https://whynottest.madcore.ru/en/site/index?action=1',
];
