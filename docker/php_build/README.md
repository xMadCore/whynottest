FROM [php:7.4.11-fpm-alpine](https://github.com/docker-library/php/blob/c257144e4bf191d6bb12927c8fd1f3df6ea6c570/7.4/alpine3.12/fpm/Dockerfile)

```
apk add
     --no-cache                 Read uncached index from network

      -t, --virtual NAME        Instead of adding all the packages to 'world', create a new virtual package with the listed dependencies and
                                add that to 'world'; the actions of the command are easily reverted by deleting the virtual package
```

```
$PHPIZE_DEPS = autoconf dpkg-dev dpkg file g++ gcc libc-dev make pkgconf re2c
```


`oniguruma-dev` нужна для `mbstring` , но вроде как уже есть


del packages
    libtool
    для пхп php7-zip php7-intl
    уже установлены libpng libpng-dev? --без libpng libpng-dev билдится нормально // libpng нужен для php7-gd, imagemagick, freetype
    del libpng-dev??
        autoconf g++ make

    --with-gd
    --with-png-dir=/usr/include/

    для php
    docker-php-ext-install curl mbstring pdo xml

    не понял, зачем он pcre-dev
	

add libpq? //postgres
add zlib-dev a.k. zlib1g-dev? //gzip compression requaired for gd 7.4


docker build --pull --no-cache -t --tag myimage:version .
docker-compose build --no-cache --pull
docker-compose up -d

https://pkgs.alpinelinux.org/packages -- package search 
https://pkgs.alpinelinux.org/package/edge/main/x86_64/libtool -- libtool

x-debug?
https://github.com/yiisoft/yii2-docker/blob/master/php/Dockerfile-debian
https://stackoverflow.com/questions/8141407/install-pecl-modules-without-the-prompts/8154466#8154466

https://www.php.net/manual/en/image.installation.php -- install GD
https://www.php.net/manual/en/mbstring.installation.php -- mbstring (just for comparison)

https://hub.docker.com/_/php/
https://github.com/docker-library/docs/blob/master/php/README.md#supported-tags-and-respective-dockerfile-links
https://github.com/docker-library/php/blob/c257144e4bf191d6bb12927c8fd1f3df6ea6c570/7.4/alpine3.12/fpm/Dockerfile
