#!/bin/bash

docker_file="docker/docker-compose.yml"

function start {
	docker-compose --file $docker_file up -d
}

function stop {
	docker-compose --file $docker_file down
}


function main {
	case "$1" in
		start)
			start;
			;;
		stop)
			stop;
			;;
		*)
			echo "Usage: $0 {start|stop}"
			exit 1
			;;
	esac
}

main $1