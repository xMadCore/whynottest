#!/bin/bash

./php "php docker/php_build/composer-setup.php --filename composer --install-dir=."
./composer require "fxp/composer-asset-plugin"
./php "php composer install"
./php "php init --env=Docker --overwrite=y"
dev_tools/init_db.sh
./devmod.sh on
./yii dev/fillfake
