#!/bin/bash

docker_file="docker/docker-compose.yml"

container_name=$1
shift
docker-compose --file $docker_file exec $container_name "$@"
