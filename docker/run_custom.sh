#!/bin/bash

function run {
	docker/run_into.sh "$@"
}

function main {
	case "$0" in
		"./codecept")
			run php vendor/bin/codecept "$@";
			;;
		"./composer")
			run php composer "$@";
			;;
		"./mysql")
			run mysql mysql -u root -proot -D ucmir;
			;;
		"./php")
			run php "$@";
			;;
		*)
			affix="-> \e[1;32mdocker/run_custom.sh\e[0m"
			echo "For using this script, create any of the next symlinks for this file:"
			echo -e "\e[36m./codecept\e[0m $affix"
			echo -e "\e[36m./composer\e[0m $affix"
			echo -e "\e[36m./hdesk-mysql\e[0m $affix"
			echo -e "\e[36m./hdesk-php\e[0m $affix"
			echo ""
			exit 1
			;;
	esac
}

main $1