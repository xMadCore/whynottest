#!/bin/bash

ln -sf docker/run_custom.sh codecept
ln -sf docker/run_custom.sh composer
ln -sf docker/run_custom.sh mysql
ln -sf docker/run_custom.sh php
ln -sf docker/serve.sh serve